# piscine-utils

## .vscode

VSCode folder that allows you to easily debug your code using the visual debugger.

You should put this at the root of your project.

Don't forget to add it to your .gitignore to avoid accidentally pushing it to the vogsphere!

## .gitignore

Standard `.gitignore` for 42s projects

## aliases

Here are some useful aliases:

```bash
# compile avec les flags strict utilises par la moulinette
alias moulinette='gcc -Wall -Wextra -Werror '
# moulinette qui affiche les erreurs mais compile quand meme
alias moulinettex='gcc -Wall -Wextra '
# compile tous les fichiers c d'un projet pour verifier si y'a des erreurs
# relatives au C
alias supermoulinette="find . -name '*.c' -print -exec gcc -Wall -Wextra -Werror -c {} ';'"
```
